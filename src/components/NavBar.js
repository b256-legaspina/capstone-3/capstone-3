import { useContext } from "react";
import { Container, Navbar, Nav, Offcanvas } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../userContext";

export default function NavBar() {
  const { user } = useContext(UserContext);

  return (
    <>
      {["sm"].map((expand) => (
        <Navbar key={expand} bg="light" expand={expand} className="mb-3">
          <Container fluid>
            <Navbar.Brand as={Link} to="/">
              Kompyuteran Atbp.
            </Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${expand}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                  Kompyuteran
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Nav className="justify-content-end flex-grow-1 pe-3">
                  <Nav.Link as={NavLink} to="/">
                    Home
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/products">
                    Products
                  </Nav.Link>

                  {user.id !== null ? (
                    <Nav.Link as={NavLink} to={"/logout"}>
                      Logout
                    </Nav.Link>
                  ) : (
                    <>
                      <Nav.Link as={NavLink} to={"/login"}>
                        Login
                      </Nav.Link>
                      <Nav.Link as={NavLink} to="/register">
                        Register
                      </Nav.Link>
                    </>
                  )}
                </Nav>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </>
  );
}
