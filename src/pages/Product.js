import { useState, useEffect } from "react";
import CourseCard from "../components/CourseCard";

export default function Products() {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    fetch("http://localhost:4000/products/active")
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);

        setProducts(
          data.map((product) => {
            return <CourseCard key={product.id} productProp={product} />;
          })
        );
      });
  });

  return (
    <>
      <h1>Products</h1>
      {products}
    </>
  );
}
