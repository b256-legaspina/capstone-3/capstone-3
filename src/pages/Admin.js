import { Row, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

export default function Admin() {
  //   const { user } = useContext(UserContext);
  const navigate = useNavigate();

  return (
    <div>
      <Row>
        <h1>Hello Admin!</h1>
      </Row>
      <Link to={"/createProduct"}>
        <Button variant="primary">Create Product</Button>{" "}
      </Link>
      <Link to={"/all"}>
        <Button variant="secondary">Retrieve all Product</Button>{" "}
      </Link>
      <Link to={"/updateProduct"}>
        <Button variant="success">Update Product Information</Button>{" "}
      </Link>
      <Link to={"/deactivate"}>
        <Button variant="danger">Deactivate Product</Button>{" "}
      </Link>
    </div>
  );
}
