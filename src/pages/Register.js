import { useContext, useEffect, useState } from "react";
import Swal from "sweetalert2";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../userContext";

export default function Register() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  //state hooks
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [number, setNumber] = useState("");
  const [password, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  console.log(firstName);
  console.log(lastName);
  console.log(number);
  console.log(email);
  console.log(password);
  console.log(password2);

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      number.length === 11 &&
      password !== "" &&
      password2 !== "" &&
      password === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, number, email, password, password2]);

  function registerUser(e) {
    e.preventDefault();

    fetch("http://localhost:4000/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === true) {
          Swal.fire({
            title: "Duplicate Email Found",
            icon: "error",
            text: "Please use another email",
          });
        } else {
          fetch("http://localhost:4000/users/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              number: number,
              password: password,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                setFirstName("");
                setLastName("");
                setEmail("");
                setNumber("");
                setPassword1("");
                setPassword2("");

                Swal.fire({
                  title: "Registered Successfully!",
                  icon: "success",
                  text: "Thank you for registering",
                });
                navigate("/login");
              } else {
                Swal.fire({
                  title: "Something Wrong",
                  icon: "error",
                  text: "Please try again",
                });
              }
            });
        }
      });
  }

  // return (
  //   user.email !== null ? (
  //     <Navigate to="/" />
  //   ) : (
  // return user.email !== null ? (
  //   <Navigate to="/" />
  // ) : (
  return (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="First Name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Last Name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicNumber">
        <Form.Label>Contact Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter number"
          value={number}
          onChange={(e) => setNumber(e.target.value)}
          minLength={11}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword1(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="formBasicVerifyPassword">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
        />
      </Form.Group>
      {/* Ternary Operator
        
        if (isActive === true) {
            <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
        } else {
            <Button variant="danger" type="submit" id="submitBtn">
          Submit
        </Button>
        }

      */}
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
