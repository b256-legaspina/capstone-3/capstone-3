import { useState, useContext, useEffect } from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import UserContext from "../userContext";
import { useNavigate, useParams, Link } from "react-router-dom";
import Swal from "sweetalert2";

export default function ProductView() {
  const { user } = useContext(UserContext);

  const { productId } = useParams();

  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  useEffect(() => {
    console.log(productId);

    fetch(`http://localhost:4000/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  });

  const purchase = (productId) => {
    fetch(`http://localhost:4000/orders/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productId: productId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Added to Cart",
            icon: "success",
            text: "Thank you!",
          });

          navigate("/products");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>
              <Card.Subtitle>Buy</Card.Subtitle>
              <Card.Text>Now!</Card.Text>
              {user.id !== null ? (
                <Button
                  variant="primary"
                  block
                  onClick={() => purchase(productId)}
                >
                  Purchase
                </Button>
              ) : (
                <Link className="btn btn-danger btn-block" to="/login">
                  Login
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
