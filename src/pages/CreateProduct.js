import { useContext, useEffect, useState } from "react";
import UserContext from "../userContext";
import { useNavigate, useNavigation, Navigate } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function CreateProduct() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  //state hooks

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [isActive, setIsActive] = useState(false);

  console.log(name);
  console.log(description);
  console.log(price);

  useEffect(() => {
    if (name !== "" && description !== "" && price !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  function createProduct(e) {
    e.preventDefault();

    fetch("http://localhost:4000/products/addProduct", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          setName("");
          setDescription("");
          setPrice("");

          Swal.fire({
            title: "Product Added!",
            icon: "success",
            text: "You can add again!",
          });
        }
      });
  }
  return (
    //   user.isAdmin !== true ? (
    //     <navigate to="/products" />
    //   ) : (
    <Form onSubmit={(e) => createProduct(e)}>
      <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>Product Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Product Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          placeholder="Description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="text"
          placeholder="Price"
          value={price}
          onChange={(e) => setPrice(e.target.value)}
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
