// import logo from './logo.svg';
import "./App.css";
import { useEffect, useState } from "react";
import { UserProvider } from "./userContext";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import NavBar from "./components/NavBar";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Product from "./pages/Product";
import ProductView from "./pages/ProductView";
import Logout from "./pages/Logout";
import Admin from "./pages/Admin";
import CreateProduct from "./pages/CreateProduct";
import RetrieveProduct from "./pages/RetrieveProduct";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  });
  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <NavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Product />} />
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/productView/:productId" element={<ProductView />} />
            <Route path="/admin" element={<Admin />} />
            <Route path="/createProduct" element={<CreateProduct />} />
            <Route path="/all" element={<RetrieveProduct />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
